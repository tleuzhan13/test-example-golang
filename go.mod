module gitlab.com/xp2pay/coinwallet

go 1.13

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/ethereum/go-ethereum v1.9.10
	github.com/gin-gonic/gin v1.5.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.2.1
	go.uber.org/zap v1.13.0 // indirect
)
