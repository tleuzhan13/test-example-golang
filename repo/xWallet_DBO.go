package repo

import (
	"context"
	"gitlab.com/xp2pay/coinwallet/db/ethClient"
	"gitlab.com/xp2pay/coinwallet/db/mongoClient"
	"gitlab.com/xp2pay/coinwallet/domain"
	"gitlab.com/xp2pay/coinwallet/utils"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	XWalletRepo XWalletRepository
)

func init() {
	XWalletRepo = &xWalletRepository{}
}

type XWalletRepository interface {
	Store(xWallet *domain.XWallet) utils.ApplicationError
	GetXWalletsByOwnerId(string) ([]*domain.XWallet, utils.ApplicationError)
	GetXWalletByOwnerIdAndTicker(string, string) (*domain.XWallet, utils.ApplicationError)
	SendCoins(string, float64, string, *domain.KeyStore, string) utils.ApplicationError
	GetBalanceFromNode(*domain.KeyStore, string) (float64, utils.ApplicationError)
}

type xWalletRepository struct{}

func (r xWalletRepository) Store(xWallet *domain.XWallet) utils.ApplicationError {
	collection, err2 := mongoClient.GetXWalletDBCollection("wallets")
	if err2 != nil {
		return err2
	}
	_, err := collection.InsertOne(context.TODO(), xWallet)
	if err != nil {
		return utils.NewInternalServerError("cannot add wallet to mongo db")
	}
	return nil
}

func (r xWalletRepository) GetXWalletsByOwnerId(owner string) ([]*domain.XWallet, utils.ApplicationError) {
	collection, err2 := mongoClient.GetXWalletDBCollection("wallets")
	if err2 != nil {
		return nil, err2
	}
	var xWallets []*domain.XWallet
	documentCur, err := collection.Find(context.TODO(), bson.M{"owner": owner})
	if err != nil {
		return nil, utils.NewInternalServerError("cannot find wallet for owner: " + owner)
	}
	for documentCur.Next(context.TODO()) {
		var xWallet domain.XWallet
		err = documentCur.Decode(&xWallet)
		if err != nil {
			return nil, utils.NewInternalServerError("cannot decode wallet for owner: " + owner)
		}
		xWallets = append(xWallets, &xWallet)
	}

	return xWallets, nil
}

func (r xWalletRepository) GetXWalletByOwnerIdAndTicker(owner string, ticker string) (*domain.XWallet, utils.ApplicationError) {
	collection, err2 := mongoClient.GetXWalletDBCollection("wallets")
	if err2 != nil {
		return nil, err2
	}
	var xWallet domain.XWallet
	document := collection.FindOne(context.TODO(), bson.M{"owner": owner, "ticker": ticker})
	err := document.Decode(&xWallet)
	if err != nil {
		return nil, utils.NewInternalServerError("cannot decode wallet for owner: " + owner + " and ticker: " + ticker)
	}
	return &xWallet, nil
}

func (r xWalletRepository) SendCoins(address string, amount float64, ticker string, keyStore *domain.KeyStore, pin string) utils.ApplicationError {
	if keyStore.Pin != pin {
		return utils.NewBadRequestError("Invalid pin code!")
	}
	_, err2 := r.GetXWalletByOwnerIdAndTicker(keyStore.Owner, ticker)
	if err2 != nil {
		return err2
	}
	switch ticker {
	case "ETH":
		err2 = ethClient.SendCoin(amount, keyStore, address)
		if err2 != nil {
			return err2
		}
		balance, err2 := ethClient.GetETHBalance(keyStore)
		if err2 != nil {
			return err2
		}
		err2 = UpdateBalance(balance, keyStore.Owner, ticker)
		if err2 != nil {
			return err2
		}
		return nil
	default:
		return utils.NewBadRequestError("Send Transaction for coin" + ticker + " is not supported")
	}

	return nil
}

func (r xWalletRepository) GetBalanceFromNode(keyStore *domain.KeyStore, ticker string) (float64, utils.ApplicationError) {
	switch ticker {
	case "ETH":
		balance, err := ethClient.GetETHBalance(keyStore)
		if err != nil {
			return 0, err
		}
		err = UpdateBalance(balance, keyStore.Owner, ticker)
		if err != nil {
			return 0, err
		}
		return balance, nil
	default:
		return 0, utils.NewBadRequestError("Get Balance from Node for coin" + ticker + " is not supported")
	}
}

func UpdateBalance(balance float64, owner, ticker string) utils.ApplicationError {
	collection, err2 := mongoClient.GetXWalletDBCollection("wallets")
	if err2 != nil {
		return err2
	}
	filter := bson.D{{"owner", owner}, {"ticker", ticker}}
	update := bson.D{
		{"$set", bson.D{
			{"balance", balance},
		}},
	}
	_, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return utils.NewInternalServerError("Cannot update balance!")
	}
	return nil
}
