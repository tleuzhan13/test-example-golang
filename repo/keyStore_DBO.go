package repo

import (
	"context"
	"gitlab.com/xp2pay/coinwallet/db/mongoClient"
	"gitlab.com/xp2pay/coinwallet/domain"
	"gitlab.com/xp2pay/coinwallet/utils"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	KeyStoreRepo KeyStoreRepository
)

func init() {
	KeyStoreRepo = &keyStoreRepository{}
}

type KeyStoreRepository interface {
	Store(keyStore *domain.KeyStore) utils.ApplicationError
	GetKeyStoreByOwnerId(owner string) (*domain.KeyStore, utils.ApplicationError)
}

type keyStoreRepository struct{}

func (r keyStoreRepository) Store(keyStore *domain.KeyStore) utils.ApplicationError {
	collection, err2 := mongoClient.GetXWalletDBCollection("keystores")
	if err2 != nil {
		return err2
	}
	_, err := collection.InsertOne(context.TODO(), keyStore)
	if err != nil {
		return utils.NewInternalServerError("cannot add keystore to mongo db")
	}
	return nil
}

func (r keyStoreRepository) GetKeyStoreByOwnerId(owner string) (*domain.KeyStore, utils.ApplicationError) {
	collection, err2 := mongoClient.GetXWalletDBCollection("keystores")
	if err2 != nil {
		return nil, err2
	}
	var keyStore domain.KeyStore
	document := collection.FindOne(context.TODO(), bson.M{"owner": owner})

	err := document.Decode(&keyStore)
	if err != nil {
		return nil, nil /*utils.NewInternalServerError("cannot decode keyStore for owner: " + owner)*/
	}
	return &keyStore, nil
}
