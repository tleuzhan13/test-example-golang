package eth

import (
	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/xp2pay/coinwallet/domain"
	"gitlab.com/xp2pay/coinwallet/utils"
)

func GenerateKeyPairs() (string, string, utils.ApplicationError) {
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		return "", "", utils.NewInternalServerError("cannot generate key pairs")
	}
	privateKeyBytes := crypto.FromECDSA(privateKey)
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return "", "", utils.NewInternalServerError("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
	}
	publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)
	return hexutil.Encode(privateKeyBytes), hexutil.Encode(publicKeyBytes), nil
}

func GetAddressFromKeyPairs(store *domain.KeyStore) (string, utils.ApplicationError) {
	privateKeyBytes, err2 := hexutil.Decode(store.PrivateKey)
	if err2 != nil {
		return "", utils.NewInternalServerError("cannot decode private key")
	}
	privateKey, err := crypto.ToECDSA(privateKeyBytes)
	if err != nil {
		return "", utils.NewInternalServerError("cannot convert private key to ecdsa")
	}
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return "", utils.NewInternalServerError("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
	}
	address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
	return address, nil
}

func CreateEthWallet(keyStore *domain.KeyStore, owner string, ticker string) (*domain.XWallet, utils.ApplicationError) {
	address, err := GetAddressFromKeyPairs(keyStore)
	if err != nil {
		return nil, err
	}
	newEthWallet := &domain.XWallet{
		Owner:   owner,
		Ticker:  ticker,
		Address: address,
		Balance: 0,
	}
	return newEthWallet, nil
}
