package services

import (
	"gitlab.com/xp2pay/coinwallet/domain"
	"gitlab.com/xp2pay/coinwallet/repo"
	"gitlab.com/xp2pay/coinwallet/services/eth"
	"gitlab.com/xp2pay/coinwallet/utils"
)

type xwalletService struct{}

var (
	XWalletService xwalletService
)

func (*xwalletService) GetWallets(owner string) ([]*domain.XWallet, utils.ApplicationError) {
	wallets, err := repo.XWalletRepo.GetXWalletsByOwnerId(owner)
	if err != nil {
		return nil, err
	}
	return wallets, nil
}

func (*xwalletService) GetWalletByTicker(owner string, ticker string) (*domain.XWallet, utils.ApplicationError) {
	xWallet, err := repo.XWalletRepo.GetXWalletByOwnerIdAndTicker(owner, ticker)
	if err != nil {
		return nil, err
	}
	return xWallet, nil
}
func (*xwalletService) SendCoins(address string, amount float64, ticker, owner, pin string) utils.ApplicationError {
	keyStore, err2 := repo.KeyStoreRepo.GetKeyStoreByOwnerId(owner)
	if err2 != nil {
		return err2
	}
	err := repo.XWalletRepo.SendCoins(address, amount, ticker, keyStore, pin)
	if err != nil {
		return err
	}
	return nil
}
func (*xwalletService) CreateWallet(owner, pin, ticker string) utils.ApplicationError {
	keyStore, err := repo.KeyStoreRepo.GetKeyStoreByOwnerId(owner)
	if err != nil {
		return err
	}
	if keyStore == nil {
		privateKey, publicKey, err := eth.GenerateKeyPairs()
		if err != nil {
			return err
		}
		keyStore = &domain.KeyStore{
			Owner:      owner,
			Pin:        pin,
			PublicKey:  publicKey,
			PrivateKey: privateKey,
		}
		if err := repo.KeyStoreRepo.Store(keyStore); err != nil {
			return err
		}
	} else {
		_, err := repo.XWalletRepo.GetXWalletByOwnerIdAndTicker(owner, ticker)
		if err == nil {
			return utils.NewBadRequestError("Wallet is already exist for " + ticker + ".")
		}
	}

	switch ticker {
	case "ETH":
		newEthWallet, err := eth.CreateEthWallet(keyStore, owner, ticker)
		if err != nil {
			return err
		}
		err = repo.XWalletRepo.Store(newEthWallet)
		if err != nil {
			return err
		}
		return nil
	default:
		return utils.NewBadRequestError("Wallet for coin" + ticker + " is not supported")
	}
}

func (*xwalletService) GetBalanceFromNode(owner, ticker string) (float64, utils.ApplicationError) {
	keyStore, err := repo.KeyStoreRepo.GetKeyStoreByOwnerId(owner)
	if err != nil {
		return 0, err
	}
	balance, err := repo.XWalletRepo.GetBalanceFromNode(keyStore, ticker)
	if err != nil {
		return 0, err
	}
	return balance, nil
}
