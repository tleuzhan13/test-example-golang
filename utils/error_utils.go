package utils

import (
	"net/http"
)

type ApplicationError interface {
	Status() int
	Message() string
	Error() string
}

type applicationError struct {
	Estatus  int    `json:"status"`
	Emessage string `json:"message"`
	Eerror   string `json:"error,omitempty"`
}

func (e *applicationError) Status() int {
	return e.Estatus
}
func (e *applicationError) Message() string {
	return e.Emessage
}
func (e *applicationError) Error() string {
	return e.Eerror
}

func NewInternalServerError(message string) ApplicationError {
	return &applicationError{
		Estatus:  http.StatusInternalServerError,
		Emessage: message,
	}
}

func NewNotFoundError(message string) ApplicationError {
	return &applicationError{
		Estatus:  http.StatusNotFound,
		Emessage: message,
	}
}

func NewBadRequestError(message string) ApplicationError {
	return &applicationError{
		Estatus:  http.StatusBadRequest,
		Emessage: message,
	}
}
