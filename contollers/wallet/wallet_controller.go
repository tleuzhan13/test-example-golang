package wallet

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xp2pay/coinwallet/services"
	"gitlab.com/xp2pay/coinwallet/utils"
	"net/http"
)

func GetWallets(context *gin.Context) {
	owner := context.Param("user_id")

	wallets, err := services.XWalletService.GetWallets(owner)
	if err != nil {
		utils.RespondError(context, err)
		return
	}
	utils.Respond(context, http.StatusOK, wallets)
}

func GetWalletsByTicker(context *gin.Context) {
	owner := context.Param("user_id")
	ticker := context.Param("coin_ticker")

	wallets, err := services.XWalletService.GetWalletByTicker(owner, ticker)
	if err != nil {
		utils.RespondError(context, err)
		return
	}
	utils.Respond(context, http.StatusOK, wallets)
}

type NewWallet struct {
	Owner  string `json:"user_id"`
	Pin    string `json:"pin_code"`
	Ticker string `json:"ticker"`
}

func CreateWallet(context *gin.Context) {
	var newWallet NewWallet
	err2 := context.ShouldBindJSON(&newWallet)
	if err2 != nil {
		restErr := utils.NewBadRequestError("invalid json body")
		utils.RespondError(context, restErr)
		return
	}

	err := services.XWalletService.CreateWallet(newWallet.Owner, newWallet.Pin, newWallet.Ticker)
	if err != nil {
		utils.RespondError(context, err)
		return
	}
	utils.Respond(context, http.StatusOK, "A new wallet for "+newWallet.Ticker+" was created.")
}

func GetBalanceFromNode(context *gin.Context) {
	owner := context.Param("user_id")
	ticker := context.Param("coin_ticker")

	balance, err := services.XWalletService.GetBalanceFromNode(owner, ticker)
	if err != nil {
		utils.RespondError(context, err)
		return
	}
	utils.Respond(context, http.StatusOK, balance)
}

type NewTransaction struct {
	Address string  `json:"address"`
	Amount  float64 `json:"amount"`
	Ticker  string  `json:"ticker"`
	Owner   string  `json:"user_id"`
	Pin     string  `json:"pin_code"`
}

func SendCoins(context *gin.Context) {
	var newTransaction NewTransaction
	err2 := context.ShouldBindJSON(&newTransaction)
	if err2 != nil {
		restErr := utils.NewBadRequestError("invalid json body")
		utils.RespondError(context, restErr)
		return
	}

	err := services.XWalletService.SendCoins(newTransaction.Address, newTransaction.Amount, newTransaction.Ticker, newTransaction.Owner, newTransaction.Pin)
	if err != nil {
		utils.RespondError(context, err)
		return
	}
	utils.Respond(context, http.StatusOK, "Transaction was completed successfully.")
}
