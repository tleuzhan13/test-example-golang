package app

import (
	"gitlab.com/xp2pay/coinwallet/contollers/ping"
	"gitlab.com/xp2pay/coinwallet/contollers/wallet"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)
	router.GET("/api/xwallet/owner/:user_id/wallets", wallet.GetWallets)
	router.GET("/api/xwallet/owner/:user_id/wallets/:coin_ticker", wallet.GetWalletsByTicker)
	router.GET("/api/xwallet/owner/:user_id/balance/:coin_ticker", wallet.GetBalanceFromNode)
	router.POST("/api/xwallet/create", wallet.CreateWallet)
	router.POST("/api/xwallet/send", wallet.SendCoins)
	//router.GET("/users/:user_id", users.GetUser)
	/*router.PUT("/users/:user_id", users.Update)
	router.PATCH("/users/:user_id", users.Update)
	router.DELETE("/users/:user_id", users.Delete)
	router.GET("/internal/users/search", users.Search)
	router.POST("/users/login", users.Login)*/
}
