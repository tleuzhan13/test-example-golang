package mongoClient

import (
	"context"
	"gitlab.com/xp2pay/coinwallet/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func GetMongoClient() (*mongo.Client, utils.ApplicationError) {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		return nil, utils.NewInternalServerError("cannot access to MongoDB")
	}
	return client, nil
}

func GetXWalletDBCollection(collectionName string) (*mongo.Collection, utils.ApplicationError) {
	client, err := GetMongoClient()
	if err != nil {
		return nil, err
	}
	collection := client.Database("xwalletdb").Collection(collectionName)
	return collection, nil
}
