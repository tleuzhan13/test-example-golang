package ethClient

import (
	"context"
	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/xp2pay/coinwallet/domain"
	"gitlab.com/xp2pay/coinwallet/utils"
	"math/big"
)

var (
	wei    = float64(1000000000000000000)
	ticker = "ETH"
)

func GetETHClient() (*ethclient.Client, utils.ApplicationError) {
	client, err := ethclient.Dial("https://rinkeby.infura.io/v3/1222a1d0d827488983a2d10d5b554e25")
	if err != nil {
		return nil, utils.NewInternalServerError("Cannot connect to ETH client!")
	}
	return client, nil
}

func SendCoin(amount float64, keyStore *domain.KeyStore, address string) utils.ApplicationError {
	client, err2 := GetETHClient()
	if err2 != nil {
		return err2
	}
	myPrivateKey, err := crypto.HexToECDSA(keyStore.PrivateKey[2:])
	if err != nil {
		return utils.NewInternalServerError("Cannot convert a private key.")
	}
	fromAddress, err2 := getETHAddress(client, myPrivateKey)
	if err2 != nil {
		return err2
	}
	nonce, err := client.PendingNonceAt(context.Background(), *fromAddress)
	if err != nil {
		return utils.NewInternalServerError("Cannot get a nonce.")
	}
	value := big.NewInt(int64(amount * wei)) // in wei (1 eth)
	gasLimit := uint64(21000)

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		return utils.NewInternalServerError("Cannot get a gas price.")
	}

	toAddress := common.HexToAddress(address)

	tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, nil)

	chainID, err := client.NetworkID(context.Background())
	if err != nil {
		return utils.NewInternalServerError("Cannot get a chain ID.")
	}

	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainID), myPrivateKey)
	if err != nil {
		return utils.NewInternalServerError("Cannot sign a transaction.")
	}

	err = client.SendTransaction(context.Background(), signedTx)
	if err != nil {
		return utils.NewInternalServerError("Transaction failed.")
	}
	return nil
}

func getETHAddress(client *ethclient.Client, myPrivateKey *ecdsa.PrivateKey) (*common.Address, utils.ApplicationError) {
	publicKey := myPrivateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, utils.NewInternalServerError("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
	}
	address := crypto.PubkeyToAddress(*publicKeyECDSA)
	return &address, nil
}

func GetETHBalance(keyStore *domain.KeyStore) (float64, utils.ApplicationError) {
	client, err2 := GetETHClient()
	if err2 != nil {
		return 0, err2
	}
	privateKey, err := crypto.HexToECDSA(keyStore.PrivateKey[2:])
	if err != nil {
		return 0, utils.NewInternalServerError("Cannot convert a private key.")
	}
	address, err2 := getETHAddress(client, privateKey)
	if err2 != nil {
		return 0, err2
	}
	curBallance, err := client.BalanceAt(context.Background(), *address, nil)
	if err != nil {
		return 0, utils.NewInternalServerError("Cannot get actual balance.")
	}
	fbalance, _ := (new(big.Float).SetInt(curBallance)).Float64()

	return fbalance / wei, nil
}
