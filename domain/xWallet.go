package domain

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type XWallet struct {
	ID      primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	Owner   string             `json:"owner" bson:"owner"`
	Ticker  string             `json:"ticker" bson:"ticker"`
	Address string             `json:"address" bson:"address"`
	Balance float64            `json:"balance" bson:"balance"`
}
