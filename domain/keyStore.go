package domain

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type KeyStore struct {
	ID         primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	Owner      string             `json:"owner" bson:"owner"`
	PublicKey  string             `json:"public_key" bson:"public_key"`
	PrivateKey string             `json:"private_key" bson:"private_key"`
	Pin        string             `json:"pin" bson:"pin"`
}
